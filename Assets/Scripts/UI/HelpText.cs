using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using TMPro;
public class HelpText : MonoBehaviour
{
    public static HelpText instance;
    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;

    private void Start()
    {
        instance = this;
    }

    public void ChangeScore(int x, int y)
    {
        text1.text = "x" + x.ToString();
        text2.text = "x" + y.ToString();
    }
}
