using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeBarController : MonoBehaviour
{

    [SerializeField] float timeInterval;
    [SerializeField] float timeOutPercent;
    [SerializeField] Image barFrontgourndImage;
    [SerializeField] GameObject focus;

    private float timeRemaining;
    private bool isSoundPlayed;
    private bool unLoad;
    private AudioSource audioSource;


    private void Start()
    {
        timeRemaining = timeInterval;
        audioSource = GetComponent<AudioSource>();
        isSoundPlayed = false;
    }


    private void Update()
    {
        if(timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;

            barFrontgourndImage.fillAmount = timeRemaining / timeInterval;


            if(timeRemaining < timeOutPercent*timeInterval)
            {
                barFrontgourndImage.color = Color.red;

                if(!isSoundPlayed)
                {
                    focus.SetActive(true);
                    isSoundPlayed = true;
                    audioSource.Play();
                }
            }
        }
        else 
        {
            timeRemaining = timeInterval;
            focus.SetActive(false);
            audioSource.Stop();
            PauseController pauseController = FindObjectOfType<PauseController>();
            pauseController.loadLoseMenu();
        }
    }
}
