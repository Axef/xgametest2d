using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class MenuController : MonoBehaviour
{   


    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    public void play()
    {
        SceneManager.LoadScene("level1");
    }
    public void quit()
    {
        Application.Quit();
    }
}
