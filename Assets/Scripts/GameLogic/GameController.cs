using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameLogic gameLogic;
    private CellBackground background;

    private void Start()
    {
        gameLogic = GetComponent<GameLogic>();
        background = GetComponent<CellBackground>();
    }


    private void Update()
    {
        getInput();
    }


    private void getInput()
    {
        if (Input.GetMouseButtonDown (0) && !PauseController.isPause) 
        {    
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f)) 
            {
                if(hit.collider.tag == "Icon") 
                {
                    gameLogic.selectIcon(hit.collider.GetComponent<IconController>());                     
                }
            }    
        }
    }
}
