using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public static GameLogic instance;

    [SerializeField] Transform parent; // vi tri cua cell khi sinh ra
    [SerializeField] GameObject cellBackground; // background cua icon
    [SerializeField] GameObject[] iconList; // danh sach icon pikachu
    [SerializeField] int numberOfIcon; // so luong icon co trong level


    [Space]
    public Vector2 fallDirection;
    public int[,] mapMatrix; // mang 2 chieu
    public IconController[,] iconMatrix; // mang 2 chieu icon cua pokemon
    public int row; // so luong hang
    public int col; // so luong cot
    public int winCount;


    private int[,] visited;
    private bool isFound;
    private List<Vector2> listFound; 
    private List<List<IconController>> idMatrix;

    private IconController previousSelect;

    public int helpCount = 0;
    public int swapCount = 0;

    private bool isMute; // bien kiem soat viec tat/mo am thanh

    private void Awake()
    {

        previousSelect = null;
        initMatrix();
        initIcons();
        winCount = col * row;
    }
    private void Start()
    {
        instance = this;
        isMute = false;
        Time.timeScale = 1f;
    }

    public void Update()
    {
        // check so luong de win
        if (winCount <= 0)
        {
            PauseController pauseController = FindObjectOfType<PauseController>();
            pauseController.loadWinMenu();
        }

        for (int i = 1; i <= row; i++)
        {
            for (int j = 1; j <= col; j++)
            {
                if (iconMatrix[i, j] == null)
                {
                    mapMatrix[i, j] = -1;
                }
            }
        }
    }
    // khoi tao ma tran
    public void initMatrix()
    {
        mapMatrix = new int[row + 2, col + 2];

        for (int i = 0; i <= row + 1; i++)
        {
            for (int j = 0; j <= col + 1; j++)
            {
                mapMatrix[i, j] = -1;
            }
        }

    }
    
    // khoi tao ma tran icon pokemon
    public void initIcons()
    {
        int maxDuplicate = row * col / numberOfIcon + 1;
        int[] countDuplicate = new int[numberOfIcon + 1];

        List<Vector2> remainPosition = new List<Vector2>(); // tao danh sach vi tri cua icon
        idMatrix = new List<List<IconController>>(); // tao danh sach icon

        // luu vao danh sach icon
        for (int i = 0; i <= numberOfIcon; i++)
        {
            List<IconController> listIndex = new List<IconController>();
            idMatrix.Add(listIndex);
        }
        // luu vi tri cua icon
        for (int i = 1; i <= row; i++)
        {
            for (int j = 1; j <= col; j++)
            {
                remainPosition.Add(new Vector2(i, j));
            }
        }

        int index = 0;
        iconMatrix = new IconController[row + 1, col + 1];

        do
        {
            int randIconIndex = Random.Range(0, numberOfIcon); // ramdom icon trong list icon pokemon
            if(countDuplicate[randIconIndex] < maxDuplicate)
            {
                // tao 2 icon de dam bao luon co 1 cap icon
                countDuplicate[randIconIndex] += 2;
                for(int j=0;j<2;j++)
                {
                    int randIndex = Random.Range(0, remainPosition.Count);
                    int x = (int)remainPosition[randIndex].x;
                    int y = (int)remainPosition[randIndex].y;


                    Vector3 axes = new Vector3(y*2.3f  , (row - x)* 2.3f, 0);

                    //khoi tao sprite icon
                    IconController icon = Instantiate(iconList[randIconIndex], axes, Quaternion.identity, parent).GetComponent<IconController>();
                    Instantiate(cellBackground, axes, Quaternion.identity, icon.GetComponent<Transform>());
                    icon.setVectorIndex(new Vector2(x, y));
                    icon.setId(randIconIndex);

                    iconMatrix[x, y] = icon; // gan vi tri cho icon trong iconMatrix

                    mapMatrix[x, y] = randIconIndex;
                    idMatrix[randIconIndex].Add(icon);

                    remainPosition.RemoveAt(randIndex);


                }
                index++;

            }   

        }
        while (index < row * col / 2);
    }

    // thuat toan Nhanh va Can
    private void findPath(Vector2 start, Vector2 target)
    {
        visited = new int[row + 2, col + 2];
        for (int i = 0; i <= row + 1; i++)
        {
            for (int j = 0; j <= col + 1; j++)
            {
                visited[i, j] = 0;
            }
        }

        isFound = false;

        listFound = new List<Vector2>();

        Vector2 vectorFound = recusive(start, start, target, 0);
        if (vectorFound != Vector2.zero)
        {
            listFound.Add(vectorFound);
        }
    }

    // de quy tim doi tuong giong
    private Vector2 recusive(Vector2 previous, Vector2 current, Vector2 target, int rotateCount)
    {
        if (isFound)
        {
            return Vector2.zero;
        }
        // 4 huong xung quanh
        int[] dx = { 1, -1, 0, 0 };
        int[] dy = { 0, 0, 1, -1 };

        visited[(int)current.x, (int)current.y] = -1;

        for (int i = 0; i < 4; i++)
        {
            Vector2 next = current + new Vector2(dx[i], dy[i]);
            // bat 2 doi tuong ko bi ngoai gioi han
            // tim den khi nao co the an hoac la k co icon nao
            if (isLegal(next, target, rotateCount + collinear(previous, current, next)))
            {
                // du kieu dien an
                if (next.x == target.x && next.y == target.y) // check 2 icon sat nhau
                {
                    isFound = true;
                    // add vi tri cua 2 icon giong nhau
                    listFound.Add(target);
                    return current;
                }
                else
                {
                    // cong them don vi nua de check tiep
                    Vector2 vectorFound = recusive(current, next, target, rotateCount + collinear(previous, current, next));
                    if (vectorFound != Vector2.zero)
                    {
                        listFound.Add(vectorFound);
                        return current;
                    }
                }
            }
        }


        visited[(int)current.x, (int)current.y] = 0;
        return Vector2.zero;
    }

    private int collinear(Vector2 previous, Vector2 current, Vector2 next)
    {
        if (Vector2.Distance(previous, next) == Vector2.Distance(previous, current) + Vector2.Distance(current, next))
        {
            return 0;
        }
        return 1;
    }

    // kiem tra cac dieu kien de an duoc 2 icon
    private bool isLegal(Vector2 next, Vector2 target, int rotateCount)
    {

        if (next == target && rotateCount < 3)
        {
            return true;
        }


        // vuot qua gioi han map
        if (next.x < 0 || next.y < 0 || next.x > row + 1 || next.y > col + 1)
        {
            return false;
        }

        // bi chan boi object khac
        if (mapMatrix[(int)next.x, (int)next.y] != -1)
        {
            return false;
        }

        // khong ton tai
        if (visited[(int)next.x, (int)next.y] == -1)
        {
            return false;
        }

        // nhieu hon 3 duong
        if (rotateCount > 2)
        {
            return false;
        }


        return true;
    }
    public void selectIcon(IconController icon)
    {
        Vector2 currentSelect = icon.getVectorIndex();
        if (previousSelect != null)
        {
            // bam icon 2 lan
            if (iconMatrix[(int)currentSelect.x, (int)currentSelect.y].getSelect())
            {
                if (!isMute)
                {
                    AudioManager.instance.UnSelected();
                }

                previousSelect.GetComponent<Transform>().GetChild(0).GetComponent<CellBackground>().setSelect(false);
                previousSelect = null;
                iconMatrix[(int)currentSelect.x, (int)currentSelect.y].setSelect(false);
            }
            else
            {
                findPath(currentSelect, previousSelect.getVectorIndex());

                if (isFound && icon.getId() == previousSelect.getId())
                {
                    if (!isMute)
                    {
                        AudioManager.instance.DestroyIcon();
                    }

                    Vector3[] listPosition = new Vector3[listFound.Count];
                    for (int i = 0; i < listFound.Count; i++)
                    {
                        listPosition[i] = new Vector3(listFound[i].y * 3, (row - listFound[i].x) * 3, 0);
                    }


                    icon.GetComponent<Transform>().GetChild(0).GetComponent<CellBackground>().setSelect(true);

                    icon.removeIcon();
                    previousSelect.removeIcon();
                    previousSelect = null;
                }
                else
                {
                    if (!isMute)
                    {
                        AudioManager.instance.WrongSelect();
                    }
                };
            }
        }
        // truong hop chua chon icon nao trong list
        else
        {
            if (!isMute)
            {
                AudioManager.instance.Selected();
            }
            previousSelect = icon;
            previousSelect.GetComponent<Transform>().GetChild(0).GetComponent<CellBackground>().setSelect(true);
            iconMatrix[(int)currentSelect.x, (int)currentSelect.y].setSelect(true);
        }
    }
    public void help()
    {
        if(helpCount>0)
        {
            helpPlayer();
            helpCount--;
            HelpText.instance.ChangeScore(helpCount,swapCount);
        }
    }
    private void helpPlayer()
    {
        if (previousSelect != null)
        {
            previousSelect.GetComponent<Transform>().GetChild(0).GetComponent<CellBackground>().setSelect(false);
            iconMatrix[(int)previousSelect.getVectorIndex().x, (int)previousSelect.getVectorIndex().y].setSelect(false);
            previousSelect = null;
        }

        for (int i = 0; i <= numberOfIcon; i++)
        {
            for (int j = 0; j < idMatrix[i].Count; j++)
            {
                if (idMatrix[i][j] == null)
                {
                    continue;
                }
                for (int k = j + 1; k < idMatrix[i].Count; k++)
                {
                    if (idMatrix[i][k] == null)
                    {
                        continue;
                    }
                    findPath(idMatrix[i][j].getVectorIndex(), idMatrix[i][k].getVectorIndex());
                    if (isFound)
                    {
                        selectIcon(idMatrix[i][j]);
                        selectIcon(idMatrix[i][k]);
                        return;
                    }
                }
            }
        }
    }
    public void Swapping()
    {
        if(swapCount > 0)
        {
            shuffle();
            swapCount--;
            HelpText.instance.ChangeScore(helpCount, swapCount);
        }    
    }    
    private void shuffle()
    {
        if (previousSelect != null)
        {
            previousSelect.GetComponent<Transform>().GetChild(0).GetComponent<CellBackground>().setSelect(false);
            iconMatrix[(int)previousSelect.getVectorIndex().x, (int)previousSelect.getVectorIndex().y].setSelect(false);
            previousSelect = null;
        }

        List<IconController> remainIcon = new List<IconController>();

        for (int i = 1; i <= row; i++)
        {
            for (int j = 1; j <= col; j++)
            {
                if (iconMatrix[i, j] != null && mapMatrix[i, j] != -1)
                {
                    remainIcon.Add(iconMatrix[i, j]);
                }
            }
        }


        while (remainIcon.Count > 2)
        {
            int randSelect1 = Random.Range(0, remainIcon.Count - 1);
            int randSelect2 = Random.Range(0, remainIcon.Count - 1);

            while (randSelect2 == randSelect1)
            {
                randSelect2 = Random.Range(0, remainIcon.Count - 1);

            }

            Vector2 position1 = remainIcon[randSelect1].getVectorIndex();
            Vector2 position2 = remainIcon[randSelect2].getVectorIndex();

            remainIcon[randSelect1].swapPosition(position2);
            remainIcon[randSelect2].swapPosition(position1);

            remainIcon.RemoveAt(randSelect1);
            remainIcon.RemoveAt(randSelect2);
        }
    }
    public void newGame()
    {
        for (int i = 1; i <= row; i++)
        {
            for (int j = 1; j <= col; j++)
            {
                if (iconMatrix[i, j] != null)
                {
                    Destroy(iconMatrix[i, j].gameObject);
                }
            }
        }

        initMatrix();
        initIcons();
        winCount = col * row;
    }
    public void mute()
    {
        isMute = !isMute;
        if(isMute)
        {
            AudioManager.instance.audioSource.volume=0f;
        }   
        else
        {
            AudioManager.instance.audioSource.volume = 1f;
        }    
    }
}
