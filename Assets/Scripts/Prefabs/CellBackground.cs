using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellBackground : MonoBehaviour
{
    public Color selectColor;
    public Color baseColor;

    private bool isSelect;
    [SerializeField]
    private SpriteRenderer spriteRenderer;


    void Start()
    {
        isSelect = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void setSelect(bool isSelect)
    {
        if (this.isSelect != isSelect)
        {
            if (isSelect)
            {
                //Debug.Log("changed");
                spriteRenderer.color = selectColor;
            }
            else
            {
                //Debug.Log("changed 2");
                spriteRenderer.color = baseColor;
            }

            this.isSelect = isSelect;
        }
    }


}
