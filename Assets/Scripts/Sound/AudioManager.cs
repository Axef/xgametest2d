using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;


    public AudioSource audioSource;

    [SerializeField]
    private AudioClip selectClip, unSelected, wrongClip, destroyIcon;


    private void Start()
    {
        instance = this;
    }
    public void Selected()
    {
        audioSource.PlayOneShot(selectClip);
    }
    public void UnSelected()
    {
        audioSource.PlayOneShot(unSelected);
    }
    public void WrongSelect()
    {
        audioSource.PlayOneShot(wrongClip);
    }
    public void DestroyIcon()
    {
        audioSource.PlayOneShot(destroyIcon);
    }
}
